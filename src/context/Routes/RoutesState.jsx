import { cilSpeedometer } from "@coreui/icons";
import CIcon from "@coreui/icons-react";
import { CNavItem } from "@coreui/react-pro";
import { useReducer } from "react";
import { SET_LOADING, SET_ROUTES } from "../types";
import RoutesContext from "./RoutesContext";
import RoutesReducer from "./routesReducer";

const RoutesState = (props) => {
  const initialState = {
    loading: true,
    routes: [
      {
        component: CNavItem,
        to: "project-list",
        icon: <CIcon icon={cilSpeedometer} customClassName='nav-icon' />,
        name: "sssلیست پروژه ها",
      },
    ],
  };

  const [state, dispatch] = useReducer(RoutesReducer, initialState);

  // Set Routes
  const setRoutes = (type) => {
    if (type === "project-manager") {
      const routes = [
        {
          component: CNavItem,
          to: "new-project",
          icon: <CIcon icon={cilSpeedometer} customClassName='nav-icon' />,
          name: "ساخت پروژه جدید",
        },
        {
          component: CNavItem,
          to: "team-members",
          icon: <CIcon icon={cilSpeedometer} customClassName='nav-icon' />,
          name: "افزودن اعضای تیم",
        },
        {
          component: CNavItem,
          to: "Target-Community-And-Customers",
          icon: <CIcon icon={cilSpeedometer} customClassName='nav-icon' />,
          name: "تعریف مشتریان و جامعه هدف",
        },
        {
          component: CNavItem,
          to: "problem-definition",
          icon: <CIcon icon={cilSpeedometer} customClassName='nav-icon' />,
          name: "تعریف مشکل",
        },
        {
          component: CNavItem,
          to: "solution-definition",
          icon: <CIcon icon={cilSpeedometer} customClassName='nav-icon' />,
          name: "تعریف راه حل",
        },
        {
          component: CNavItem,
          to: "minimal-viable-product",
          icon: <CIcon icon={cilSpeedometer} customClassName='nav-icon' />,
          name: "طراحی اولیه محصول قابل اجرا",
        },
     
      ];
      dispatch({ type: SET_ROUTES, payload: routes });
    } else if (type === "super-admin") {
      const routes = [
        {
          component: CNavItem,
          to: "manage-projects",
          icon: <CIcon icon={cilSpeedometer} customClassName='nav-icon' />,
          name: "مدیریت پروژه ها",
        },
        {
          component: CNavItem,
          to: "managers-list",
          icon: <CIcon icon={cilSpeedometer} customClassName='nav-icon' />,
          name: "لیست مدیر محصول ها",
        },
        {
          component: CNavItem,
          to: "add-manager",
          icon: <CIcon icon={cilSpeedometer} customClassName='nav-icon' />,
          name: "افزودن مدیر محصول",
        },
      ];

      dispatch({ type: SET_ROUTES, payload: routes });
    }
  };

  // Set Loading
  const setLoading = () => {
    dispatch({ type: SET_LOADING });
  };

  return (
    <RoutesContext.Provider
      value={{
        routes: state.routes,
        loading: state.loading,
        setRoutes,
        setLoading,
      }}
    >
      {props.children}
    </RoutesContext.Provider>
  );
};

export default RoutesState;
