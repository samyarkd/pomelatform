import { CContainer, CSpinner } from "@coreui/react-pro";
import React, { Suspense, useContext } from "react";
import { Navigate, Route, Routes } from "react-router-dom";
import AuthContext from "../../../context/auth/AuthContext";
import { AddManager } from "../../pages/app/addManager/AddManager";
import MVP from "../../pages/app/editProject/MVP";
import TeamMembers from "../../pages/app/editProject/TeamMembers";
import ManageProjects from "../../pages/app/manageProjects/ManageProjects";
import ManagersList from "../../pages/app/managersList/ManagersList";
import NewProject from "../../pages/app/newProject/NewProject";
import ProjectPage from "../../pages/app/projectPage/ProjectPage";
import SignOffer from "../../pages/app/signOffer/SignOffer";
import ProblemDefinition from "./../../pages/app/editProject/ProblemDefinition";
import SolutionDefinition from "./../../pages/app/editProject/SolutionDefinition";
import TargetCC from "./../../pages/app/editProject/TargetCommunityAndCustomers";

const NavigateTo = () => {
  const { user } = useContext(AuthContext);

  if (user?.role_name) {
    if (user.role_name === "super-admin") {
      return <Navigate to='manage-projects' />;
    } else {
      return <Navigate to='projects' />;
    }
  }
};

function AppContent() {
  const { user } = useContext(AuthContext);

  return (
    <CContainer className='py-3 px-5' fluid>
      <Suspense fallback={<CSpinner color='primary' />}>
        <Routes>
          {user?.role_name === "super-admin" ? (
            <>
              <Route path='manage-projects' element={<ManageProjects />} />
              <Route path='managers-list' element={<ManagersList />} />
              <Route path='add-manager' element={<AddManager />} />
              <Route path='project/:id' element={<ProjectPage />} />
              <Route path='project/add-offer/:id' element={<SignOffer />} />
            </>
          ) : (
            <>
              <Route path='new-project' element={<NewProject />} />
              <Route path='team-members' element={<TeamMembers />} />
              <Route
                path='Target-Community-And-Customers'
                element={<TargetCC />}
              />
              <Route
                path='problem-definition'
                element={<ProblemDefinition />}
              />
              <Route
                path='solution-definition'
                element={<SolutionDefinition />}
              />
              <Route path='minimal-viable-product' element={<MVP />} />
            </>
          )}
          <Route path='*' element={<NavigateTo />} />
        </Routes>
      </Suspense>
    </CContainer>
  );
}

export default AppContent;
