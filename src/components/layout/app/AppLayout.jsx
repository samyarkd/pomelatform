import AppContent from "../AppContent/AppContent";
import Footer from "../footer/Footer";
import Header from "../header/Header";
import Sidebar from "../sidebar/Sidebar";

function AppLayout() {
  return (
    <div className='flex min-h-screen flex-row-reverse '>
      <Sidebar />
      <div className='wrapper flex-grow flex flex-col justify-between bg-neutral-200 w-full'>
        <div>
          <Header />
          <AppContent />
        </div>
        <Footer />
      </div>
    </div>
  );
}

export default AppLayout;
