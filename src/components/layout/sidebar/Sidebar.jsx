import {
  CSidebar,
  CSidebarBrand,
  CSidebarHeader,
  CSidebarNav
} from "@coreui/react-pro";
import React, { useContext } from "react";
import RoutesContext from "./../../../context/Routes/RoutesContext";
import SidebarContext from "./../../../context/Sidebar/SidebarContext";
import { AppSidebarNav } from "./SidebarNav";

function Sidebar() {
  const { visible } = useContext(SidebarContext);
  const { routes } = useContext(RoutesContext);
  return (
    <CSidebar visible={visible} position='fixed'>
      <CSidebarHeader>
        <CSidebarBrand className='bg-transparent font-bold h-full select-none'>
          Pomelatform
        </CSidebarBrand>
      </CSidebarHeader>

      <CSidebarNav>
        <AppSidebarNav items={routes} />
      </CSidebarNav>
    </CSidebar>
  );
}

export default Sidebar;
