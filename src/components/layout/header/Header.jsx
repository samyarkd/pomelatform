import { cilMenu } from "@coreui/icons";
import CIcon from "@coreui/icons-react";
import { CContainer, CHeader, CHeaderNav, CHeaderToggler } from "@coreui/react-pro";
import React, { useContext } from "react";
import AuthContext from "../../../context/auth/AuthContext";
import SidebarContext from "./../../../context/Sidebar/SidebarContext";
import HeaderDropDown from "./HeaderDropDown";

function Header() {
  const { toggleSidebar } = useContext(SidebarContext);
  const { user } = useContext(AuthContext);

  return (
    <>
      <CHeader position='sticky'>
        <CContainer fluid>
          <CHeaderToggler
            onClick={() => {
              toggleSidebar();
            }}
            className='ps-1'
          >
            <CIcon icon={cilMenu} size='lg' />
          </CHeaderToggler>
          <CHeaderNav>
            <HeaderDropDown />
            <div className='flex flex-col justify-between items-start'>
              <span>{user?.name}</span>
              <span className='text-gray-400 text-sm'>Role</span>
            </div>
          </CHeaderNav>
        </CContainer>
      </CHeader>
    </>
  );
}

export default Header;
