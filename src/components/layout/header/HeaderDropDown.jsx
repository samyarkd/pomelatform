import { cilAccountLogout } from "@coreui/icons";
import CIcon from "@coreui/icons-react";
import {
  CAvatar,
  CDropdown,
  CDropdownHeader,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle
} from "@coreui/react-pro";
import React, { useContext } from "react";
import AuthContext from "../../../context/auth/AuthContext";
import RoutesContext from "../../../context/Routes/RoutesContext";

function HeaderDropDown() {
  const { logout, user } = useContext(AuthContext);
  const { setLoading } = useContext(RoutesContext);
  return (
    <CDropdown variant='nav-item'>
      <CDropdownToggle placement='bottom-end' className='py-0' caret={false}>
        <CAvatar color='dark' size='md' />
      </CDropdownToggle>
      <CDropdownMenu className='pt-0' placement='bottom-end'>
        <CDropdownHeader className='bg-light fw-semibold py-2'>
          {user?.name}
        </CDropdownHeader>

        <CDropdownItem
          onClick={() => {
            logout();
            setLoading();
          }}
          className='mt-1'
          href='#'
        >
          Logout
          <CIcon icon={cilAccountLogout} className='me-2' />
        </CDropdownItem>
      </CDropdownMenu>
    </CDropdown>
  );
}

export default HeaderDropDown;
