import { CCard, CCardBody, CCardHeader, CCardTitle } from "@coreui/react-pro";
import React from "react";

function InputDiv({ text }) {
  return (
    <div className='flex flex-col md:flex-row md:w-6/12 w-full gap-2 md:rounded '>
      <label className='text-sm  w-40 whitespace-nowrap justify-center bg-blue-500 min-h-full py-2 md:rounded-r rounded px-7 text-center flex items-center text-white font-bold'>
        {text}{" "}
      </label>
      <input
        placeholder={text}
        className='shadow-none flex-1 h-10 rounded md:rounded-l border-0 bg-slate-100'
        type='text'
      />
    </div>
  );
}

function NewProject() {
  const handleSubmit = (e) => {
    e.preventDefault();
  };
  return (
    <CCard>
      <CCardHeader>
        <CCardTitle>پروژه جدید</CCardTitle>
      </CCardHeader>
      <CCardBody>
        <form
          onSubmit={handleSubmit}
          className='flex flex-col justify-center items-center gap-y-10'
        >
          <CCardTitle className='font-bold'>ایجاد پروژه جدید</CCardTitle>
          <InputDiv text='نام و نام خانوادگی نماینده' />
          <InputDiv text='نام مدیر محصول' />
          <InputDiv text='نام پروژه' />
          <InputDiv text='نام انگلیسی پروژه' />

          <div className='flex flex-col md:flex-row gap-2 md:w-6/12 w-full rounded '>
            <label className='w-40 whitespace-nowrap justify-center bg-blue-500 min-h-full rounded py-2 px-7 text-center flex items-center text-white font-bold'>
              خلاصه پروژ
            </label>
            <textarea
              placeholder='خلاصه پروژه ...'
              className='shadow-none flex-1 h-20 rounded border-0 bg-slate-100'
              type='text'
            />
          </div>

          <div className='md:w-6/12 w-full'>
            <button
              className='bg-blue-500 px-4 py-0.5 text-white font-bold rounded my-3 active:scale-[0.97] duration-100 hover:bg-blue-600 shadow-md shadow-blue-500/50 w-full h-10 '
              type='submit'
            >
              ایجاد پروژه
            </button>
          </div>
        </form>
      </CCardBody>
    </CCard>
  );
}

export default NewProject;
