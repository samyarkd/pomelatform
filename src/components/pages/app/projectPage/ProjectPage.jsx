import {
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCardTitle
} from "@coreui/react-pro";
import React from "react";
import { useParams } from "react-router-dom";

function ProjectPage() {
  const { id } = useParams();

  return (
    <CCard>
      {/*Header*/}
      <CCardHeader>
        <CCardTitle>اطلاعات پروژه</CCardTitle>
      </CCardHeader>
      {/*Project Info*/}
      <CCardBody>
        <div className='flex flex-row items-center gap-x-16'>
          <h2 className='font-semibold tracking-wide'>نام پروژه</h2>
          <span className='text-slate-500 text-xl  '>نام مدیر محصول</span>
        </div>
        <div>
          {/* Developer Team */}
          <div className='flex flex-col mt-16 md:flex-row'>
            <div className='bg-slate-600 text-base text-white sm:text-xl px-5  md:rounded-tr-md md:rounded-br-md flex items-center justify-center md:rounded-t-none rounded-t-md whitespace-nowrap w-full md:w-2/12 py-1'>
              تیم نرمافزار
            </div>
            <div className='w-full md:rounded-tl-md rounded-b-md md:rounded-b-none md:rounded-bl-md border flex p-2 flex-row items-center gap-5'>
              {/* Developer */}
              <div className='flex flex-row items-center shadow-md'>
                <div className='bg-blue-600 text-white rounded-r-sm font-bold border-l-2 p-1 text-xs px-3 flex items-center justify-center'>
                  نام
                </div>
                <div className='bg-orange-500 text-white font-bold rounded-l-sm p-1 flex items-center px-3 text-xs  justify-center'>
                  تخصص
                </div>
              </div>
            </div>
          </div>
          {/*Team*/}
          <div className='flex flex-col mt-8 md:flex-row'>
            <div className='bg-slate-600 text-base text-white sm:text-xl px-5  md:rounded-tr-md md:rounded-br-md flex items-center justify-center md:rounded-t-none rounded-t-md whitespace-nowrap w-full md:w-2/12 py-1'>
              تیم همراه
            </div>
            <div className='w-full md:rounded-tl-md rounded-b-md md:rounded-b-none md:rounded-bl-md border flex p-2 flex-row items-center gap-5'>
              {/* Team Member */}
              <div className='flex flex-row items-center shadow-md'>
                <div className='bg-blue-600 text-white rounded-r-sm font-bold border-l-2 p-1  px-3 text-xs flex items-center justify-center'>
                  نام
                </div>
                <div className='bg-orange-500 text-white font-bold rounded-l-sm p-1 flex items-center px-3 text-xs justify-center'>
                  تخصص
                </div>
              </div>
            </div>
          </div>
          {/* Project Summary */}
          <div className='flex flex-col mt-8 md:flex-row'>
            <div className='bg-slate-600 text-base text-white sm:text-xl px-5  md:rounded-tr-md md:rounded-br-md flex items-center justify-center md:rounded-t-none rounded-t-md whitespace-nowrap w-full md:w-2/12 py-1'>
              خلاصه پروژه
            </div>
            <div className='w-full md:rounded-tl-md rounded-b-md md:rounded-b-none md:rounded-bl-md border flex p-2 flex-row items-center'>
              <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae
                tempora iusto vel fuga nesciunt at cupiditate repudiandae cumque
                unde ratione mollitia voluptatum ipsa nihil laudantium ut
                architecto et, magni est.
              </p>
            </div>
          </div>
          <div className='flex flex-col mt-8 md:flex-row'>
            <div className='bg-slate-600 text-base text-white sm:text-xl px-5  md:rounded-tr-md md:rounded-br-md flex items-center justify-center md:rounded-t-none rounded-t-md whitespace-nowrap w-full md:w-2/12 py-1'>
              لینک ها
            </div>
            <div className='w-full md:rounded-tl-md rounded-b-md md:rounded-b-none md:rounded-bl-md border p-2 items-center grid-cols-2 sm:grid-cols-3 grid gap-3'>
              <div className='btn-purple'>لینک اول</div>
              <div className='btn-purple'>لینک دوم</div>
              <div className='btn-purple'>لینک سوم</div>
              <div className='btn-purple'>لینک چهارم</div>
            </div>
          </div>
        </div>
      </CCardBody>
      <CCardFooter className='mt-5'>{id}</CCardFooter>
    </CCard>
  );
}

export default ProjectPage;
