import {
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCardTitle,
} from "@coreui/react-pro";
import React, { useState } from "react";
import { useParams } from "react-router-dom";
import AddingMailStone from "./AddingMailStone";
import DealContext from "./DealContext";
import DeveloperTeam from "./DeveloperTeam";
import ProgressBar from "./ProgressBar";
import ProjectTechnologies from "./ProjectTechnologies";

function Steps({ step, state }) {
  switch (step) {
    case 1:
      return <ProjectTechnologies fState={state} />;
    case 2:
      return <DeveloperTeam fState={state} />;
    case 3:
      return <AddingMailStone fState={state} />;
    case 4:
      return <DealContext />;
    default:
      break;
  }
}

function NextPrevBtn({ formState, setFormState }) {
  const { step } = formState;
  const handleStep = (e) => {
    if (step < 4 && e.target.name === "next") {
      setFormState({
        ...formState,
        step: step + 1,
      });
    } else if (step > 1 && e.target.name === "prev") {
      setFormState({
        ...formState,
        step: step - 1,
      });
    }
  };

  return (
    <div className='flex justify-between'>
      <button
        className='shadow-md shadow-slate-300/30 border-2 border-slate-600 py-0.5 px-4 font-bold text-slate-600 rounded duration-200 hover:bg-slate-600 hover:text-white active:scale-[0.97] text-lg'
        name='prev'
        onClick={handleStep}
      >
        قبل
      </button>
      <button
        className='shadow-md shadow-blue-300/30 border-2 border-blue-500 py-0.5 px-4 font-bold text-blue-500 rounded duration-200 hover:bg-blue-500 hover:text-white active:scale-[0.97] text-lg'
        name='next'
        onClick={handleStep}
      >
        بعد
      </button>
    </div>
  );
}

const SignOffer = () => {
  const [formState, setFormState] = useState({
    step: 1,
    techs: [],
    team: [],
    mail_stones: [],
  });
  const state = { state: formState, setState: setFormState };
  const { step } = formState;
  const params = useParams();

  const { id } = params;

  return (
    <CCard>
      <CCardHeader>
        <CCardTitle>ثبت پیشنهاد</CCardTitle>
      </CCardHeader>
      <CCardBody>
        <div className='flex m-auto flex-col justify-center w-10/12'>
          <ProgressBar step={step} />
          <Steps step={step} state={state} />
          <NextPrevBtn setFormState={setFormState} formState={formState} />
        </div>
      </CCardBody>
      <CCardFooter>
        <div className='flex flex-row gap-x-7'>
          <span>
            <b> پروژه : </b> {id}
          </span>
          <span>
            <b>مدیر محصول : </b> سامیار
          </span>
        </div>
      </CCardFooter>
    </CCard>
  );
};

export default SignOffer;
