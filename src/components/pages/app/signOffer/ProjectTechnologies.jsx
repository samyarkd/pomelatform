import { CCardTitle } from "@coreui/react-pro";
import React from "react";

function ProjectTechnologies({ fState }) {
  const { state, setState } = fState;
  const { techs } = state;

  const getTechs = (e) => {
    let techsTags = e.target.value
      .split(",")
      .map((tech) => tech.trim())
      .filter((tech) => tech !== "");

    setState({
      ...state,
      techs: techsTags,
    });
  };

  return (
    <div className='flex flex-col mx-3 mt-10'>
      {/*Title*/}
      <CCardTitle className='font-bold text-2xl'>
        جزئیات مربوط به تکنولوژی
      </CCardTitle>
      {/* Body */}
      <div className='mb-5'>
        <div className='my-3 flex gap-1 flex-wrap'>
          {techs.map((tech, idx) => (
            <span
              key={idx}
              className='px-2 py-0.5 bg-slate-500 text-white rounded'
            >
              {tech}
            </span>
          ))}
        </div>
        <div className='flex flex-col mt-6 md:flex-row'>
          <div className='bg-slate-600 text-base text-white sm:text-xl px-5  md:rounded-tr-md md:rounded-br-md flex items-center justify-center md:rounded-t-none rounded-t-md whitespace-nowrap w-full md:w-2/12 py-1'>
            تکنولوژی ها
          </div>
          <div className='w-full md:rounded-tl-md rounded-b-md md:rounded-b-none md:rounded-bl-md border flex flex-row items-center'>
            <textarea
              placeholder='... React,PHP,NodeJS'
              className='text-left w-full border-none'
              name='techs'
              id='techs'
              onChange={getTechs}
            ></textarea>
          </div>
        </div>
        <span>
          تکنولوژی ها را با استفاده از کاما <b>","</b> از هم جدا کنید.
        </span>
      </div>
    </div>
  );
}

export default ProjectTechnologies;
