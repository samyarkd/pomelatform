import React, { useEffect, useState } from "react";

function ProgressBar({ step }) {
  const [lanePercentage, setLanePercentage] = useState(0);

  useEffect(() => {
    setLanePercentage(Math.floor(((step - 1) / (4 - 1)) * 100));
  }, [step]);

  function activeColor(c) {
    if (step >= c) {
      return "bg-blue-500 shadow-md shadow-blue-300";
    } else {
      return "bg-slate-500";
    }
  }

  return (
    <div className='flex flex-row justify-between items-center relative'>
      <div
        className={`before:content-[""] before:absolute before:bg-slate-200 before:w-full before:h-2 before:z-0 flex flex-row justify-between items-center w-full`}
      >
        <div
          className='progressBar bg-blue-600'
          style={{ width: lanePercentage + "%" }}
        ></div>
        <div
          className={`z-10 rounded-full ${activeColor(
            1
          )} w-12 justify-center flex items-center text-white h-12`}
        >
          <span>1</span>
        </div>
        <div
          className={`z-10 rounded-full  w-12 justify-center  flex items-center text-white h-12 ${activeColor(
            2
          )}`}
        >
          <span>2</span>
        </div>
        <div
          className={`z-10 rounded-full w-12 justify-center flex items-center text-white h-12 ${activeColor(
            3
          )}`}
        >
          <span>3</span>
        </div>
        <div
          className={`z-10 rounded-full w-12 justify-center flex items-center text-white h-12 ${activeColor(
            4
          )}`}
        >
          <span>4</span>
        </div>
      </div>
    </div>
  );
}

export default ProgressBar;
