import { CCardTitle } from "@coreui/react-pro";
import React from "react";

function DealContext() {
  return (
    <div className='flex flex-col mx-3 mt-10'>
      {/*Title*/}
      <div className='flex flex-row justify-between items-center mb-3'>
        <CCardTitle className='font-bold text-2xl'>متن توافق نامه</CCardTitle>
        <button className='rounded active:scale-[0.97] duration-100 hover:bg-green-600 shadow-md shadow-green-500/30 bg-green-500 min-h-full px-7 text-center flex items-center text-white font-bold'>
          دانلود pdf توافق نامه
        </button>
      </div>
      {/* Body */}
      <div className='mb-5'>
        <div className='w-full bg-slate-100 border-none resize-y h-72 shadow-none'></div>
      </div>
    </div>
  );
}

export default DealContext;
