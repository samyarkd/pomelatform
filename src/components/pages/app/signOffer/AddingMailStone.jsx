/* eslint-disable react-hooks/exhaustive-deps */
import {
  CCardTitle,
  CModal,
  CModalBody,
  CModalHeader,
  CModalTitle,
  CTable,
  CTableBody,
  CTableDataCell,
  CTableHead,
  CTableHeaderCell,
  CTableRow
} from "@coreui/react-pro";
import React, { useEffect, useState } from "react";

const AddMailStoneModule = ({
  listData,
  setListData,
  visible,
  setVisible,
  fData,
}) => {
  const { MailStoneData, setMailStoneData } = fData;

  const handleChange = (e) => {
    setMailStoneData({ ...MailStoneData, [e.target.name]: e.target.value });
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    if (MailStoneData.vir === true) {
      const newData = listData.map((item) => {
        if (item.id === MailStoneData.id) {
          return { ...item, ...MailStoneData, vir: false };
        }
        return item;
      });
      setListData(newData);
    } else {
      setListData([
        ...listData,
        { id: Date.now() + Math.floor(Math.random() * 1000), ...MailStoneData },
      ]);
    }
    setMailStoneData({
      id: Date.now() + Math.floor(Math.random() * 1000),
      name: "",
      deliverables: "",
      payment_amount: "",
      when_paid: "",
      when_released: "",
      payment_title: "",
      committed_work: "",
      remained_committed_money: "",
    });

    setVisible(false);
  };

  return (
    <CModal
      size='xl'
      onClose={() => {
        setVisible(false);
        setMailStoneData({
          id: Date.now() + Math.floor(Math.random() * 1000),
          name: "",
          deliverables: "",
          payment_amount: "",
          when_paid: "",
          when_released: "",
          payment_title: "",
          committed_work: "",
          remained_committed_money: "",
        });
      }}
      visible={visible}
    >
      <CModalHeader className='flex flex-row rtl-header'>
        <CModalTitle>افزودن مایل استون جدید</CModalTitle>
      </CModalHeader>
      <CModalBody>
        <form
          onSubmit={handleSubmit}
          className='w-11/12 m-auto grid md:grid-cols-2 gap-2 auto-cols-auto'
        >
          <div>
            <label className='text-sm font-bold mb-2'>نام مایل استون</label>
            <input
              className='w-full border-2 border-gray-400 rounded-lg py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-gray-500'
              onChange={handleChange}
              value={MailStoneData.name}
              type='text'
              name='name'
              placeholder='نام مایل استون'
            />
          </div>
          <div>
            <label className='text-sm font-bold mb-2'>کار متعهد شده</label>
            <input
              className='w-full border-2 border-gray-400 rounded-lg py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-gray-500'
              onChange={handleChange}
              value={MailStoneData.committed_work}
              type='text'
              name='committed_work'
              placeholder='کار متعهد شده'
            />
          </div>
          <div>
            <label className='text-sm font-bold mb-2'>تحویلی ها</label>
            <input
              className='w-full border-2 border-gray-400 rounded-lg py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-gray-500'
              onChange={handleChange}
              value={MailStoneData.deliverables}
              type='text'
              name='deliverables'
              placeholder='تحویلی ها'
            />
          </div>
          <div>
            <label className='text-sm font-bold mb-2'>عنوان پرداخت</label>
            <input
              className='w-full border-2 border-gray-400 rounded-lg py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-gray-500'
              onChange={handleChange}
              value={MailStoneData.payment_title}
              type='text'
              name='payment_title'
              placeholder='عنوان پرداخت'
            />
          </div>
          <div>
            <label className='text-sm font-bold mb-2'>مقدار سرمایه</label>
            <input
              className='w-full border-2 border-gray-400 rounded-lg py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-gray-500'
              onChange={handleChange}
              value={MailStoneData.payment_amount}
              type='text'
              name='payment_amount'
              placeholder='مقدار سرمایه'
            />
          </div>
          <div>
            <label className='text-sm font-bold mb-2'>زمان پرداخت</label>
            <input
              className='w-full border-2 border-gray-400 rounded-lg py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-gray-500'
              onChange={handleChange}
              value={MailStoneData.when_paid}
              type='text'
              name='when_paid'
              placeholder='زمان پرداخت'
            />
          </div>
          <div>
            <label className='text-sm font-bold mb-2'>سرمایه باقی مانده</label>
            <input
              className='w-full border-2 border-gray-400 rounded-lg py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-gray-500'
              onChange={handleChange}
              value={MailStoneData.remained_committed_money}
              type='text'
              name='remained_committed_money'
              placeholder='سرمایه باقی مانده'
            />
          </div>
          <div>
            <label className='text-sm font-bold mb-2'>زمان انتشار</label>
            <input
              className='w-full border-2 border-gray-400 rounded-lg py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-gray-500'
              onChange={handleChange}
              value={MailStoneData.when_released}
              type='text'
              name='when_released'
              placeholder='زمان انتشار'
            />
          </div>

          <div className='col-span-full '>
            <button
              className='bg-blue-500 text-white font-bold w-full rounded shadow-md shadow-blue-500/30 active:scale-[0.97] duration-150 py-2 text-xl my-5 '
              type='submit'
            >
              {MailStoneData?.vir ? "ویرایش" : "افزودن"}
            </button>
          </div>
        </form>
      </CModalBody>
    </CModal>
  );
};

const Table = ({
  data,
  fState: { setListData, setMailStoneData, setVisible },
}) => {
  const deleteMailStone = (id) => {
    const newData = data.filter((_, index) => index !== id);
    setListData(newData);
  };

  return (
    <CTable responsive bordered>
      <CTableHead>
        <CTableRow>
          <CTableHeaderCell scope='col'>نام مایل استون</CTableHeaderCell>
          <CTableHeaderCell scope='col'>کار متعهد شده</CTableHeaderCell>
          <CTableHeaderCell scope='col'>تحویلی ها</CTableHeaderCell>
          <CTableHeaderCell scope='col'>عنوان سرمایه</CTableHeaderCell>
          <CTableHeaderCell scope='col'>مقدار سرمایه</CTableHeaderCell>
          <CTableHeaderCell scope='col'>زمان پرداخت</CTableHeaderCell>
          <CTableHeaderCell scope='col'>سرمایه باقی مانده</CTableHeaderCell>
          <CTableHeaderCell scope='col'>زمان انتشار</CTableHeaderCell>
          <CTableHeaderCell scope='col'>عملیات</CTableHeaderCell>
        </CTableRow>
      </CTableHead>
      <CTableBody>
        {data.length === 0 ? (
          <CTableRow>
            <CTableDataCell colSpan={9}>
              <div className='text-center text-gray-500'>
                <p className='text-xl'>داده ای برای نمایش وجود ندارد</p>
              </div>
            </CTableDataCell>
          </CTableRow>
        ) : (
          data.map((item, idx) => (
            <CTableRow key={idx}>
              <CTableHeaderCell scope='row'>{item.name}</CTableHeaderCell>
              <CTableDataCell>{item.committed_work}</CTableDataCell>
              <CTableDataCell>{item.deliverables}</CTableDataCell>
              <CTableDataCell>{item.payment_title}</CTableDataCell>
              <CTableDataCell>{item.payment_amount}</CTableDataCell>
              <CTableDataCell>{item.when_paid}</CTableDataCell>
              <CTableDataCell>{item.remained_committed_money}</CTableDataCell>
              <CTableDataCell>{item.when_released}</CTableDataCell>
              <CTableDataCell className='flex items-stretch justify-center'>
                <button
                  className='btn-red'
                  onClick={() => deleteMailStone(idx)}
                >
                  حذف
                </button>
                <button
                  className='btn-blue'
                  onClick={() => {
                    setMailStoneData({ ...item, vir: true });
                    setVisible(true);
                  }}
                >
                  ویرایش
                </button>
              </CTableDataCell>
            </CTableRow>
          ))
        )}
      </CTableBody>
    </CTable>
  );
};

const AddMailStoneBtn = ({ setVisible }) => {
  return (
    <div>
      <button
        className='px-4 py-0.5  hover:bg-blue-600  bg-blue-500 text-white font-bold rounded shadow-md shadow-blue-500/30 active:scale-[0.97] duration-100 my-3'
        onClick={() => setVisible(true)}
      >
        افزدون مایل استون
      </button>
    </div>
  );
};

function AddingMailStone({ fState }) {
  const { state, setState } = fState;
  const [listData, setListData] = useState(state.mail_stones);
  const [visible, setVisible] = useState(false);
  const [MailStoneData, setMailStoneData] = useState({
    id: Date.now() + Math.floor(Math.random() * 1000),
    name: "",
    deliverables: "",
    payment_amount: "",
    when_paid: "",
    when_released: "",
    payment_title: "",
    committed_work: "",
    remained_committed_money: "",
  });
  useEffect(() => {
    setState({ ...state, mail_stones: listData });
  }, [listData]);

  return (
    <div className='flex flex-col mx-3 mt-10 pb-5'>
      {/*Title*/}
      <CCardTitle className='font-bold text-2xl'>
        افزدون مایل استون ها
      </CCardTitle>
      {/* Body */}
      <AddMailStoneBtn setVisible={setVisible} />
      <Table
        data={listData}
        fState={{
          setListData,
          setMailStoneData,
          setVisible,
        }}
      />
      <AddMailStoneModule
        listData={listData}
        setListData={setListData}
        visible={visible}
        setVisible={setVisible}
        fData={{ MailStoneData, setMailStoneData }}
      />
    </div>
  );
}

export default AddingMailStone;
