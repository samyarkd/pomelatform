import {
  CCard, CCardBody,
  CCardHeader,
  CCardTitle,
  CSmartTable
} from "@coreui/react-pro";
import { Line } from "rc-progress";
import React from "react";
import { Link } from "react-router-dom";

function ProjectList() {
  const columns = [
    {
      label: "نام پروژه",
      key: "name",
    },
    {
      label: "خلاصه",

      key: "dec",
      filter: false,
      sorter: false,
    },
    {
      label: "درصد پیشرفت",
      key: "progress",
      filter: false,
    },
    {
      key: "options",
      label: "عملیات ها",
      _style: { width: "1%" },
      filter: false,
      sorter: false,
      _props: { className: "fw-semibold" },
    },
  ];
  const usersData = [
    {
      id: 0,
      name: "John Doe",
      registered: "2018/01/01",
      role: "Guest",
      status: "Pending",
      per: 50,
    },
    {
      id: 1,
      name: "Samppa Nori",
      registered: "2018/01/01",
      role: "Member",
      status: "Active",
      per: 30,
    },
    {
      id: 2,
      name: "Estavan Lykos",
      registered: "2018/02/01",
      role: "Staff",
      status: "Banned",
      offer: true,
      per: 80,
    },
  ];

  return (
    <CCard className=''>
      <CCardHeader>
        <CCardTitle>لیست پروژه ها</CCardTitle>
      </CCardHeader>
      <CCardBody>
        <CSmartTable
          columns={columns}
          columnFilter
          columnSorter
          items={usersData}
          itemsPerPageSelect
          pagination
          itemsPerPageLabel='تعداد پروژه ها در هر صفحه :'
          scopedColumns={{
            options: (p) => {
              return (
                <td className='py-2'>
                  <div className='flex flex-row gap-x-2 '>
                    <Link
                      to={`/app/projects/${p.id}`}
                      className='no-underline text-blue-800 whitespace-nowrap border-2 border-blue-800 px-3 rounded hover:bg-blue-800 hover:text-slate-50 shadow-sm active:scale-95 font-semibold text-sm shadow-blue-800/30 group'
                    >
                      ویرایش و جزئیات بیشتر{" "}
                    </Link>
                  </div>
                </td>
              );
            },
            progress: (p) => {
              return (
                <td>
                  <div className='flex items-center'>
                    <span>{p.per}%</span>
                    <Line
                      val
                      percent={p.per}
                      strokeWidth='4'
                      strokeColor='#1e40af'
                    />
                  </div>
                </td>
              );
            },
          }}
          tableProps={{
            hover: true,
          }}
          className='hidden'
        />
      </CCardBody>
    </CCard>
  );
}

export default ProjectList;
