/* eslint-disable react-hooks/exhaustive-deps */
import { CCard, CCardBody, CCardHeader, CCardTitle } from "@coreui/react-pro";
import React, { useEffect, useState } from "react";

// function getImageUrl(file, setUrl, formData) {
//   const url = URL.createObjectURL(file);
//   setUrl({ ...formData, preview_image: url });
// }

function Input({ setFormData, formData, text, type, name, minLength }) {
  return (
    <div className='flex flex-col gap-2'>
      <label className='font-bold'>{text}</label>
      <input
        type={type}
        name={name}
        required
        value={formData[name]}
        minLength={minLength}
        onChange={(e) => setFormData({ ...formData, [name]: e.target.value })}
        placeholder={text}
        className='border-2 border-blue-500 rounded '
      />
    </div>
  );
}

function AddManager() {
  const [formData, setFormData] = useState({
    preview_image: "",
    image: "",
    name: "",
    email: "",
    password: "",
    repeat_password: "",
    disabled: true,
  });
  const { disabled, name, email, password, repeat_password, username } =
    formData;

  function submitHandler(e) {
    e.preventDefault();
  }

  useEffect(() => {
    if (
      password !== "" &&
      repeat_password !== "" &&
      email !== "" &&
      name !== "" &&
      username !== "" &&
      password === repeat_password
    ) {
      setFormData({ ...formData, disabled: false });
    }
  }, [password, repeat_password, email, name]);

  return (
    <CCard>
      <CCardHeader>
        <CCardTitle className='m-0'>افزدون مدیر محصول</CCardTitle>
      </CCardHeader>
      <CCardBody>
        <form onSubmit={submitHandler} className='flex flex-col items-center'>
          {/*  <div className='flex items-center gap-5 justify-center'>
            <CAvatar src={preview_image} size='xl' color='dark' />
            <div>
              <div className='text-red-500 font-bold'>عکس باید 1 * 1 باشد</div>
              <input
                type='file'
                onChange={(e) =>
                  getImageUrl(e.target.files[0], setFormData, formData)
                }
                accept='image/jpeg'
                className='file:border-none duration-100 file:bg-green-600 file:text-white file:rounded-sm focus:border-none outline-none file:cursor-pointer '
              />
            </div>
          </div> */}
          <div className='mt-3 grid grid-cols-1 gap-4 w-full sm:w-5/12 md:basis-6/12'>
            <Input
              setFormData={setFormData}
              formData={formData}
              minLength={1}
              text={"نام و نام خانوادگی"}
              type={"text"}
              name={"name"}
            />
            <Input
              setFormData={setFormData}
              formData={formData}
              minLength={1}
              text={"نام کاربری"}
              type={"text"}
              name={"username"}
            />
            <Input
              setFormData={setFormData}
              formData={formData}
              minLength={1}
              text={"آدرس ایمیل"}
              type={"email"}
              name={"email"}
            />
            <Input
              setFormData={setFormData}
              formData={formData}
              minLength={6}
              text={"گذرواژه"}
              type={"password"}
              name={"password"}
            />
            <Input
              setFormData={setFormData}
              formData={formData}
              minLength={6}
              text={"تکرار گذزواژه"}
              type={"password"}
              name={"repeat_password"}
            />
          </div>
          <div className='my-5 w-5/12'>
            <button
              type='submit'
              className='bg-blue-500 shadow-md shadow-blue-500/30 active:scale-[0.97] text-white w-full py-2 rounded disabled:cursor-not-allowed disabled:bg-blue-800/50'
              disabled={disabled}
            >
              ساخت مدیر محصول
            </button>
          </div>
        </form>
      </CCardBody>
    </CCard>
  );
}

export { AddManager, Input };
