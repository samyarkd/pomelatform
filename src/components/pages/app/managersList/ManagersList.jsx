import {
  CAvatar,
  CCard,
  CModal,
  CModalBody,
  CModalHeader,
  CModalTitle,
  CSmartTable
} from "@coreui/react-pro";
import React, { useState } from "react";
import { Input } from "../addManager/AddManager";

function EditManagerModal({ formData, setFormData }) {
  const inputs = [
    {
      text: "نام و نام خانوادگی",
      type: "text",
      name: "name",
      minLength: 1,
    },
    {
      text: "نام کاربری",
      type: "text",
      name: "username",
      minLength: 1,
    },
    {
      text: "آدرس ایمیل",
      type: "email",
      name: "email",
      minLength: 1,
    },
  ];

  function handleSubmit(e) {
    e.preventDefault();
    setFormData({ ...formData, disabled: false });
  }

  return (
    <CModal
      onClose={() => setFormData({ ...formData, disabled: false })}
      visible={formData.disabled}
    >
      <CModalHeader className='rtl-header'>
        <CModalTitle>ویرایش مدیرمحصول</CModalTitle>
      </CModalHeader>
      <CModalBody>
        <form onSubmit={handleSubmit} className='flex flex-col gap-y-5'>
          {inputs.map((input, idx) => (
            <Input
              key={idx}
              text={input.text}
              type={input.type}
              name={input.name}
              minLength={input.minLength}
              formData={formData}
              setFormData={setFormData}
            />
          ))}
          <div>
            <button
              type='submit'
              className='btn-blue w-full h-10 text-xl block'
            >
              ذخیره
            </button>
          </div>
        </form>
      </CModalBody>
    </CModal>
  );
}

const ManagersList = () => {
  const [formData, setFormData] = useState({
    username: "",
    name: "",
    email: "",
    disabled: false,
  });
  const columns = [
    {
      label: "پروفایل",
      key: "profile",
      filter: false,
      _props: { className: "w-1/12" },
    },
    {
      label: "نام",
      key: "name",
      _props: { className: "w-6/12 sm:w-4/12 md:w-2/12" },
    },
    {
      label: "وضعیت",
      key: "status",
      filter: false,
    },
    {
      label: "عملیات",
      key: "action",
      filter: false,
      _props: { className: "w-2/12" },
    },
  ];
  const usersData = [
    {
      id: 0,
      email: "test@exm.com",
      username: "test",
      name: "John Doe",
      registered: "2018/01/01",
      role: "Guest",
      status: "Pending",
    },
    {
      id: 1,
      email: "test@exm.com",
      username: "test",
      name: "Samppa Nori",
      registered: "2018/01/01",
      role: "Member",
      status: "Active",
    },
    {
      id: 2,
      email: "test@exm.com",
      username: "test",
      name: "Estavan Lykos",
      registered: "2018/02/01",
      role: "Staff",
      status: "Banned",
    },
  ];

  function editHandler(e, user) {
    setFormData({
      username: user.username,
      name: user.name,
      email: user.email,
      disabled: true,
    });
  }

  return (
    <CCard className='p-3'>
      <CSmartTable
        columns={columns}
        columnFilter
        columnSorter
        items={usersData}
        itemsPerPageSelect
        pagination
        itemsPerPageLabel='تعداد پروژه ها در هر صفحه :'
        tableProps={{
          hover: true,
        }}
        scopedColumns={{
          profile: (e) => (
            <td className=' flex items-center'>
              <CAvatar color='dark' />
            </td>
          ),
          status: (e) => (
            <td>
              <span
                style={{ paddingTop: "2px", paddingBottom: "2px" }}
                className={`text-white shadow-sm rounded-sm px-2  ${
                  e.status === "Pending"
                    ? "bg-orange-600 shadow-orange-700"
                    : e.status === "Active"
                    ? "shadow-green-700 bg-green-600"
                    : "shadow-red-700 bg-red-600"
                }`}
              >
                {e.status}
              </span>
            </td>
          ),
          name: (e) => (
            <td>
              <div className='align-center self-center'>{e.name}</div>
            </td>
          ),
          action: (user) => (
            <td>
              <div className='flex flex-row gap-x-4'>
                <button
                  onClick={(e) => editHandler(e, user)}
                  className='btn-blue'
                >
                  ویرایش
                </button>
                <button className='btn-red'>حذف</button>
              </div>
            </td>
          ),
        }}
        className='hidden'
      />
      <EditManagerModal formData={formData} setFormData={setFormData} />
    </CCard>
  );
};

export default ManagersList;
