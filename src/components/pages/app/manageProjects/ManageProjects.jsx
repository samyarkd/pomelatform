import { CCard, CSmartTable } from "@coreui/react-pro";
import React from "react";
import { Link } from "react-router-dom";

const ManageProjects = () => {
  const columns = [
    {
      label: "نام پروژه",
      key: "name",
      _style: { width: "20%" },
      _props: "w-3/12",
    },
    {
      label: "مدیر محصول",
      _props: "w-3/12",
      _style: { width: "20%" },
    },
    {
      label: "خلاصه",
      _style: { width: "30%" },
      _props: { className: "w-4/12 " },

      key: "dec",
      filter: false,
      sorter: false,
    },
    {
      key: "options",
      label: "عملیات ها",
      _style: { width: "1%" },
      filter: false,
      sorter: false,
      _props: { className: "fw-semibold" },
    },
  ];
  const usersData = [
    {
      id: 0,
      name: "John Doe",
      registered: "2018/01/01",
      role: "Guest",
      status: "Pending",
    },
    {
      id: 1,
      name: "Samppa Nori",
      registered: "2018/01/01",
      role: "Member",
      status: "Active",
    },
    {
      id: 2,
      name: "Estavan Lykos",
      registered: "2018/02/01",
      role: "Staff",
      status: "Banned",
      offer: true,
    },
  ];

  return (
    <CCard className='pb-3 px-3'>
      <CSmartTable
        columns={columns}
        columnFilter
        columnSorter
        items={usersData}
        itemsPerPageSelect
        pagination
        itemsPerPageLabel='تعداد پروژه ها در هر صفحه :'
        scopedColumns={{
          options: (p) => {
            return (
              <td className='py-2'>
                <div className='flex flex-row gap-x-2 '>
                  <Link
                    to={`/app/project/${p.id}`}
                    className='no-underline text-blue-800 whitespace-nowrap border-2 border-blue-800 px-3 rounded hover:bg-blue-800 hover:text-slate-50 shadow-sm active:scale-95 font-semibold text-sm shadow-blue-800/30 group'
                  >
                    جزئیات بیشتر
                  </Link>
                  <Link
                    to={`/app/project/add-offer/${p.id}`}
                    className='no-underline text-green-800 whitespace-nowrap border-2 border-green-800 rounded hover:bg-green-800 hover:text-slate-50 shadow-sm active:scale-95 font-semibold text-sm shadow-green-800/30 w-28 mx-auto text-center'
                  >
                    {p?.offer ? "ویرایش پیشنهاد" : "افزودن پیشنهاد"}
                  </Link>
                </div>
              </td>
            );
          },
        }}
        tableProps={{
          hover: true,
        }}
        className='hidden'
      />
    </CCard>
  );
};

export default ManageProjects;
