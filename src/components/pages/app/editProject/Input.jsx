import React from "react";

function Input({ guideLink, guideText, templateLink }) {
  return (
    <div className='my-5'>
      <div className='flex justify-end gap-x-8 my-3'>
        <button className='border-2 border-blue-500 text-blue-500 hover:bg-blue-500 hover:text-white font-bold px-3 rounded duration-100 active:scale-[0.97]'>
          دانلود تمپلیت
        </button>
        <button className='border-2 border-blue-500 text-blue-500 hover:bg-blue-500 hover:text-white font-bold px-3 rounded duration-100 active:scale-[0.97]'>
          {guideText}
        </button>
      </div>
      <div className='flex items-stretch border-2 border-green-600 rounded'>
        <label className='bg-green-600 min-h-full px-7 text-center flex items-center text-white font-bold '>
          ثبت لینک
        </label>
        <input
          className='shadow-none flex-1 h-10 rounded-l border-0 bg-slate-100'
          type='text'
          name='persona'
        />
      </div>
    </div>
  );
}

export default Input;
