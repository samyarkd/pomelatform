import { CCard, CCardBody, CCardHeader, CCardTitle } from "@coreui/react-pro";
import React from "react";
import Input from "./Input";

function ProblemDefinition() {
  return (
    <CCard>
      {/*Header*/}
      <CCardHeader className='flex flex-row justify-between'>
        {/*Title*/}
        <CCardTitle className='font-bold text-2xl'>تعریف مشکل</CCardTitle>
        <button className='btn-green w-40 text-lg'>ذخیره</button>
      </CCardHeader>
      {/* Body */}
      <CCardBody className='flex flex-col'>
        <div className='mb-5'>
          <p>
            لطفا فایل اسکریپت مصاحبه ، نتایج مصاحبه و بوم چشم انداز مورد نظرتان
            را در فولدر گوگل درایو آپلود کنید و لینک آن را ثبت نمایید
          </p>
          <Input guideText='راهنمای تکمیل فایل اسکریپت مصاحبه' />
          <Input guideText='راهنمای تکمیل فایل نتایج مصاحبه' />
          <Input guideText='راهنمای تکمیل فایل بوم چشم انداز' />
        </div>
      </CCardBody>
    </CCard>
  );
}

export default ProblemDefinition;
