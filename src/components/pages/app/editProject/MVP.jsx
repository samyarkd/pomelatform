import { CCard, CCardBody, CCardHeader, CCardTitle } from "@coreui/react-pro";
import React from "react";
import Input from "./Input";

function MVP() {
  return (
    <CCard>
      {/*Header*/}
      <CCardHeader className='flex flex-row justify-between'>
        {/*Title*/}
        <CCardTitle className='font-bold text-2xl'>
          طراحی اولیه محصول قابل اجرا
        </CCardTitle>
        <button className='btn-green w-40 text-lg'>ذخیره</button>
      </CCardHeader>
      {/* Body */}
      <CCardBody className='flex flex-col'>
        <div className='mb-5'>
          <p>
            لطفا فایل اپیک ها و استوری ها، یوزر فلو و بک لاگ محصول مورد نظرتان
            را در فولدر گوگل درایو آپلود کنید و لینک آن را ثبت نمایید
          </p>
          <Input guideText='راهنمای تکمیل فایل  اپیک ها و استوریهای کاربر' />
          <Input guideText='راهنمای تکمیل فایل  فایل User Flow' />
          <Input guideText='راهنمای تکمیل فایل فایل بک لاگ محصول' />
        </div>
      </CCardBody>
    </CCard>
  );
}

export default MVP;
