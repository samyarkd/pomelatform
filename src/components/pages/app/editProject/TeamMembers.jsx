import { CCard, CCardBody, CCardHeader, CCardTitle } from "@coreui/react-pro";
import React, { useState } from "react";

function TeamMembers() {
  const [form, setForm] = useState({ name: "", role: "", email: "" });
  const [team, setTeam] = useState([]);
  const handleSubmit = (e) => {
    e.preventDefault();
    setTeam([...team, form]);
    setForm({ name: "", role: "" });
  };

  const handleForm = (e) => {
    const { name, value } = e.target;
    setForm({
      ...form,
      [name]: value,
    });
  };

  const handleRemove = (index) => {
    setTeam(team.filter((_, i) => i !== index));
  };

  return (
    <CCard>
      <CCardHeader className='flex flex-row justify-between'>
        {/*Title*/}
        <CCardTitle className='font-bold text-2xl'>اعضای تیم</CCardTitle>
        {/* Save */}
        <button className='btn-green w-40 text-lg'>ذخیره</button>
      </CCardHeader>
      <CCardBody className='flex flex-col mx-3 mt-10'>
        {/* Body */}
        <div className='mb-5'>
          {/* Add new member to team */}
          <form
            onSubmit={handleSubmit}
            className='flex flex-wrap flex-row gap-4'
          >
            <div className='flex flex-col gap-1'>
              <label>
                {" "}
                <b className='text-red-500'>*</b> نام
              </label>
              <input
                required
                onChange={handleForm}
                placeholder='مریم'
                type='text'
                name='name'
                value={form.name || ""}
                className='rounded'
              />
            </div>
            <div className='flex flex-col gap-1'>
              <label>
                {" "}
                <b className='text-red-500'>*</b> نقش{" "}
              </label>
              <input
                required
                name='role'
                onChange={handleForm}
                placeholder='مدیر محصول'
                type='text'
                value={form.role || ""}
                className='rounded'
              />
            </div>
            <div className='flex flex-col gap-1'>
              <label>
                {" "}
                <b className='text-red-500'>*</b> ایمیل{" "}
              </label>
              <input
                required
                name='email'
                onChange={handleForm}
                placeholder='info@email.com'
                type='text'
                value={form.email || ""}
                className='rounded'
              />
            </div>
            <button
              className='self-end rounded bg-green-600 font-bold py-2 shadow-md shadow-green-300/30 px-4 text-white'
              type='submit'
            >
              اضافه کردن
            </button>
          </form>
          {/* Team members box */}
          <div className='flex flex-col mt-8 md:flex-row'>
            <div className='bg-slate-600 text-base text-white sm:text-xl px-5  md:rounded-tr-md md:rounded-br-md flex items-center justify-center md:rounded-t-none rounded-t-md whitespace-nowrap w-full md:w-2/12 py-1'>
              اعضای تیم
            </div>
            <div className='w-full md:rounded-tl-md rounded-b-md md:rounded-b-none md:rounded-bl-md border flex-wrap flex p-2 flex-row items-center gap-y-2 gap-x-5'>
              {/* Team Member */}
              {team.map((member, index) => (
                <div
                  key={index}
                  className='flex flex-row items-center shadow-md relative'
                >
                  <div
                    onClick={() => handleRemove(index)}
                    className='cursor-pointer hover:scale-150 duration-100 rounded-full h-3 w-3 absolute -top-1.5 -left-1.5 bg-red-600 text-white text-xs p-2 flex justify-center items-center '
                  >
                    X
                  </div>
                  <div className='bg-blue-600 text-white rounded-r-sm font-bold border-l-2 p-1  px-3 text-xs flex items-center justify-center'>
                    {member.name}
                  </div>
                  <div className='bg-orange-500 text-white font-bold rounded-l-sm p-1 flex items-center px-3 text-xs justify-center'>
                    {member.role}
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </CCardBody>
    </CCard>
  );
}

export default TeamMembers;
