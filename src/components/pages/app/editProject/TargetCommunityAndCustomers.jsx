import { CCard, CCardBody, CCardHeader, CCardTitle } from "@coreui/react-pro";
import React from "react";
import Input from "./Input";

function TargetCC() {
  return (
    <CCard>
      {/*Header*/}
      <CCardHeader className='flex flex-row justify-between'>
        {/*Title*/}
        <CCardTitle className='font-bold text-2xl'>
          تعریف مشتریان و جامعه هدف
        </CCardTitle>
        <button className='btn-green w-40 text-lg'>ذخیره</button>
      </CCardHeader>
      {/* Body */}
      <CCardBody className='flex flex-col '>
        <div className='mb-5'>
          <p>
            لطفا فایل پرسونا و نقشه همدلی مورد نظرتان را در فولدر گوگل درایو
            آپلود کنید و لینک آن را ثبت نمایید.
          </p>
          <Input
            guideLink=''
            guideText='راهنمای تکمیل فایل پرسونا'
            templateLink=''
          />
          <Input
            guideLink=''
            guideText='راهنمای تکمیل نقشه همدلی'
            templateLink=''
          />
        </div>
      </CCardBody>
    </CCard>
  );
}

export default TargetCC;
