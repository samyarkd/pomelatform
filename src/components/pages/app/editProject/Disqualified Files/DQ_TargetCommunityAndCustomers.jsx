import { CCardTitle } from "@coreui/react-pro";
import React from "react";
import Input from "./Input";

function TargetCC() {
  return (
    <div className='flex flex-col mx-3 mt-10'>
      {/*Title*/}
      <CCardTitle className='font-bold text-2xl'>
        تعریف مشتریان و جامعه هدف
      </CCardTitle>
      {/* Body */}
      <div className='mb-5'>
        <p>
          لطفا فایل پرسونا و نقشه همدلی مورد نظرتان را در فولدر گوگل درایو آپلود
          کنید و لینک آن را ثبت نمایید.
        </p>
        <Input
          guideLink=''
          guideText='راهنمای تکمیل فایل پرسونا'
          templateLink=''
        />
        <Input
          guideLink=''
          guideText='راهنمای تکمیل نقشه همدلی'
          templateLink=''
        />
      </div>
    </div>
  );
}

export default TargetCC;
