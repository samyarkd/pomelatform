import { CCardTitle } from "@coreui/react-pro";
import React from "react";
import Input from "./Input";

function MVP() {
  return (
    <div className='flex flex-col mx-3 mt-10'>
      {/*Title*/}
      <CCardTitle className='font-bold text-2xl'>
        طراحی اولیه محصول قابل اجرا
      </CCardTitle>
      {/* Body */}

      <div className='mb-5'>
        <p>
          لطفا فایل اپیک ها و استوری ها، یوزر فلو و بک لاگ محصول مورد نظرتان را
          در فولدر گوگل درایو آپلود کنید و لینک آن را ثبت نمایید
        </p>
        <Input guideText='راهنمای تکمیل فایل  اپیک ها و استوریهای کاربر' />
        <Input guideText='راهنمای تکمیل فایل  فایل User Flow' />
        <Input guideText='راهنمای تکمیل فایل فایل بک لاگ محصول' />
      </div>
    </div>
  );
}

export default MVP;
