import { CCardTitle } from "@coreui/react-pro";
import React from "react";
import Input from "./Input";

function SolutionDefinition() {
  return (
    <div className='flex flex-col mx-3 mt-10'>
      {/*Title*/}
      <CCardTitle className='font-bold text-2xl'>تعریف راه حل</CCardTitle>
      {/* Body */}

      <div className='mb-5'>
        <p>
          لطفا فایل الگوبرداری از راهکارهای موجود، تعریف راهکار با حداقل امکانات
          در سطح رقبا، فرضیات متمایزکننده و نتایج مصاحبه راهکار با مشتریان مورد
          نظرتان را در فولدر گوگل درایو آپلود کنید و لینک آن را ثبت نمایید
        </p>
        <Input guideText='راهنمای تکمیل فایل بوم چشم انداز' />
        <Input guideText='راهنمای تکمیل فایل تعریف راهکار با حداقل امکانات در سطح رقبا' />
        <Input guideText='راهنمای تکمیل فایل فرضیات متمایز کننده' />
        <Input guideText='راهنمای تکمیل فایل نتایج مصاحبه راهکار با مشتریان' />
      </div>
    </div>
  );
}

export default SolutionDefinition;
