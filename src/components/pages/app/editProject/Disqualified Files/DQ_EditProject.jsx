import {
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCardTitle
} from "@coreui/react-pro";
import React, { useState } from "react";
import { useParams } from "react-router-dom";
import MVP from "./DQ_MVP";
import ProblemDefinition from "./DQ_ProblemDefinition";
import ProgressBar from "./DQ_ProgressBar";
import SolutionDefinition from "./DQ_SolutionDefinition";
import TargetCC from "./DQ_TargetComunityAndCustomers";
import TeamMembers from "./TeamMembers";

function Steps({ step, state }) {
  switch (step) {
    case 1:
      return <TeamMembers fState={state} />;
    case 2:
      return <TargetCC fState={state} />;
    case 3:
      return <ProblemDefinition fState={state} />;
    case 4:
      return <SolutionDefinition fState={state} />;
    case 5:
      return <MVP fState={state} />;
    default:
      break;
  }
}

function NextPrevBtn({ formState, setFormState }) {
  const { step } = formState;
  const handleStep = (e) => {
    if (step < 5 && e.target.name === "next") {
      setFormState({
        ...formState,
        step: step + 1,
      });
    } else if (step > 1 && e.target.name === "prev") {
      setFormState({
        ...formState,
        step: step - 1,
      });
    }
  };

  return (
    <div className='flex justify-between'>
      <button
        className='shadow-md shadow-slate-300/30 border-2 border-slate-600 py-0.5 px-4 font-bold text-slate-600 rounded duration-200 hover:bg-slate-600 hover:text-white active:scale-[0.97] text-lg'
        name='prev'
        onClick={handleStep}
      >
        قبل
      </button>
      <button
        className='shadow-md shadow-blue-300/30 border-2 border-blue-500 py-0.5 px-4 font-bold text-blue-500 rounded duration-200 hover:bg-blue-500 hover:text-white active:scale-[0.97] text-lg'
        name='next'
        onClick={handleStep}
      >
        بعد
      </button>
    </div>
  );
}

const EditProject = () => {
  const [formState, setFormState] = useState({
    step: 1,
    techs: [],
    team: [],
    mail_stones: [],
  });
  const state = { state: formState, setState: setFormState };
  const { step } = formState;
  const params = useParams();

  const { id } = params;

  return (
    <CCard>
      <CCardHeader>
        <CCardTitle>پروژه Name</CCardTitle>
      </CCardHeader>
      <CCardBody>
        <div className='flex m-auto flex-col justify-center w-10/12'>
          <ProgressBar step={step} />
          <Steps step={step} state={state} />
          <NextPrevBtn setFormState={setFormState} formState={formState} />
        </div>
      </CCardBody>
      <CCardFooter>
        <div className='flex flex-row gap-x-7'>
          <span>
            <b> پروژه : </b> {id}
          </span>
          <span>
            <b>مدیر محصول : </b> سامیار
          </span>
        </div>
      </CCardFooter>
    </CCard>
  );
};

export default EditProject;
