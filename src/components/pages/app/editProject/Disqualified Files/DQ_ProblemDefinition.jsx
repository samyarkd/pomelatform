import { CCardTitle } from "@coreui/react-pro";
import React from "react";
import Input from "./Input";

function ProblemDefinition() {
  return (
    <div className='flex flex-col mx-3 mt-10'>
      {/*Title*/}
      <CCardTitle className='font-bold text-2xl'>تعریف مشکل</CCardTitle>
      {/* Body */}

      <div className='mb-5'>
        <p>
          لطفا فایل اسکریپت مصاحبه ، نتایج مصاحبه و بوم چشم انداز مورد نظرتان را
          در فولدر گوگل درایو آپلود کنید و لینک آن را ثبت نمایید
        </p>
        <Input guideText='راهنمای تکمیل فایل اسکریپت مصاحبه' />
        <Input guideText='راهنمای تکمیل فایل نتایج مصاحبه' />
        <Input guideText='راهنمای تکمیل فایل بوم چشم انداز' />
      </div>
    </div>
  );
}

export default ProblemDefinition;
