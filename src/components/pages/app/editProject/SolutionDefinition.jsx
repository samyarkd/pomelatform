import { CCard, CCardBody, CCardHeader, CCardTitle } from "@coreui/react-pro";
import React from "react";
import Input from "./Input";

function SolutionDefinition() {
  return (
    <CCard>
      {/*Header*/}
      <CCardHeader className='flex flex-row justify-between'>
        {/*Title*/}
        <CCardTitle className='font-bold text-2xl'>تعریف راه حل</CCardTitle>

        <button className='btn-green w-40 text-lg'>ذخیره</button>
      </CCardHeader>
      {/* Body */}
      <CCardBody className='flex flex-col'>
        <div className='mb-5'>
          <p>
            لطفا فایل الگوبرداری از راهکارهای موجود، تعریف راهکار با حداقل
            امکانات در سطح رقبا، فرضیات متمایزکننده و نتایج مصاحبه راهکار با
            مشتریان مورد نظرتان را در فولدر گوگل درایو آپلود کنید و لینک آن را
            ثبت نمایید
          </p>
          <Input guideText='راهنمای تکمیل فایل بوم چشم انداز' />
          <Input guideText='راهنمای تکمیل فایل تعریف راهکار با حداقل امکانات در سطح رقبا' />
          <Input guideText='راهنمای تکمیل فایل فرضیات متمایز کننده' />
          <Input guideText='راهنمای تکمیل فایل نتایج مصاحبه راهکار با مشتریان' />
        </div>
      </CCardBody>
    </CCard>
  );
}

export default SolutionDefinition;
