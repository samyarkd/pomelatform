/* eslint-disable react-hooks/exhaustive-deps */
import { CCard, CCardBody, CSpinner } from "@coreui/react-pro";
import React, { useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import AuthContext from "../../../context/auth/AuthContext";

function ChangeP() {
  const navigate = useNavigate();

  const [data, setData] = useState({
    email: "",
    password: "",
  });
  // const { password, r_password } = data;

  const { loading } = useContext(AuthContext);

  function submitHandler(e) {
    e.preventDefault();
  }

  useEffect(() => {
    if (localStorage.token) {
      navigate("/app/project-list");
    }
  }, []);
  return (
    <div className='bg-slate-200 h-screen w-screen flex flex-col items-center justify-evenly'>
      {/* Logo  */}
      <div
        className='absolute top-1 flex flex-col items-center justify-between gap-2'
        style={{ maxWidth: "120px" }}
      >
        <img
          alt='Logo'
          src='https://cdn0.iconfinder.com/data/icons/octicons/1024/dashboard-512.png'
        />
        <span className='font-semibold text-gray-50 text-blue-600'>
          Pomelatform
        </span>
      </div>
      <CCard className='shadow-md shadow-slate-400 w-11/12 md:w-7/12 lg:w-4/12'>
        <CCardBody>
          {/* Header */}
          <div className='mb-10'>
            <h1 className='text-3xl'>تغییر گذرواژه</h1>
          </div>
          {/* From */}
          <form onSubmit={submitHandler}>
            {/* Password input */}
            <div className='mb-5 my-3 input-div'>
              <input
                required
                placeholder=' '
                onChange={(e) => setData({ ...data, password: e.target.value })}
                type={"password"}
                className='text-right form-input w-full border-t-0 border-x-0 border-b-2  outline-none shadow-none'
              />

              <label className='select-none text-lg input-label '>
                <span>گذرواژه جدید</span>
              </label>
            </div>
            <div className='mb-5 my-3 input-div'>
              <input
                required
                placeholder=' '
                onChange={(e) =>
                  setData({ ...data, r_password: e.target.value })
                }
                type={"password"}
                className='text-right form-input w-full border-t-0 border-x-0 border-b-2  outline-none shadow-none'
              />

              <label className='select-none text-lg input-label '>
                <span>تکرار گذرواژه</span>
              </label>
            </div>
            <button
              type='submit'
              disabled={loading}
              className='w-full shadow-md shadow-blue-500 duration-100 active:scale-[0.97] bg-blue-600 text-white p-2 rounded hover:bg-blue-700'
            >
              {loading ? <CSpinner /> : "ورود"}
            </button>
          </form>
        </CCardBody>
      </CCard>
    </div>
  );
}

export default ChangeP;
