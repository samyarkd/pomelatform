/* eslint-disable react-hooks/exhaustive-deps */
import { CCard, CCardBody, CSpinner } from "@coreui/react-pro";
import React, { useContext, useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import AuthContext from "../../../context/auth/AuthContext";

const Login = () => {
  const navigate = useNavigate();

  const [data, setData] = useState({
    email: "",
    password: "",
  });
  const { email, password } = data;

  const { login, loading } = useContext(AuthContext);

  function loginHandler(e) {
    e.preventDefault();
    if (email === "" || password === "") {
      alert("لطفا همه فیلد ها را وارد نمایید !!");
    } else {
      login({
        email,
        password,
      });
    }
  }

  useEffect(() => {
    if (localStorage.token) {
      navigate("/app/project-list");
    }
  }, []);

  return (
    // login form
    <div className='bg-slate-200 h-screen w-screen flex flex-col items-center justify-evenly'>
      {/* Logo  */}
      <div
        className='absolute top-1 flex flex-col items-center justify-between gap-2'
        style={{ maxWidth: "120px" }}
      >
        <img
          alt='Logo'
          src='https://cdn0.iconfinder.com/data/icons/octicons/1024/dashboard-512.png'
        />
        <span className='font-semibold text-gray-50 text-blue-600'>
          Pomelatform
        </span>
      </div>
      {/* Card */}
      <CCard className='shadow-md shadow-slate-400 w-11/12 md:w-7/12 lg:w-4/12'>
        <CCardBody>
          {/* Header */}
          <div className='mb-10'>
            <h1 className='text-3xl'>ورود به حساب کاربری</h1>
          </div>
          {/* From */}
          <form onSubmit={loginHandler}>
            {/* Email input */}
            <div className='mb-5 my-3 input-div'>
              <input
                required
                autoComplete='email'
                placeholder=' '
                onChange={(e) => setData({ ...data, email: e.target.value })}
                type={"email"}
                className='text-right form-input w-full border-t-0 border-x-0 border-b-2  outline-none shadow-none'
              />

              <label className='select-none text-lg input-label '>
                <span>ایمیل</span>
              </label>
            </div>
            {/* Password */}
            <div className='my-3 input-div'>
              <input
                required
                placeholder=' '
                minLength='6'
                type='password'
                className='form-input w-full border-t-0 border-x-0 border-b-2  outline-none shadow-none'
                onChange={(e) => setData({ ...data, password: e.target.value })}
              />
              <label className='select-none input-label text-lg'>
                <span> گذر </span>
                <span style={{ transitionDelay: "100ms" }}>واژه</span>
              </label>
            </div>
            <div className='mt-5 mb-3'>
              <Link
                className='no-underline text-blue-900 hover:text-blue-600 font-semibold'
                to='/forgot-password'
              >
                گذرواژه خود را فراموش کرده اید ؟
              </Link>
            </div>
            <button
              type='submit'
              disabled={loading}
              className='w-full shadow-md shadow-blue-500 duration-100 active:scale-[0.97] bg-blue-600 text-white p-2 rounded hover:bg-blue-700'
            >
              {loading ? <CSpinner /> : "ورود"}
            </button>

            <div className='my-3'>
              نیاز به اکانت دارید ؟{" "}
              <Link
                className='no-underline text-blue-900 hover:text-blue-600 font-bold'
                to='/register'
              >
                ثبت نام
              </Link>
            </div>
          </form>
        </CCardBody>
      </CCard>
    </div>
  );
};

export default Login;
