/* eslint-disable react-hooks/exhaustive-deps */
import { CCard, CCardBody, CSpinner } from "@coreui/react-pro";
import React, { useContext, useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import AuthContext from "./../../../context/auth/AuthContext";

const Register = () => {
  const navigate = useNavigate();
  const [data, setData] = useState({
    email: "",
    name: "",
    password: "",
    repeatPassword: "",
  });
  const { email, name, password, repeatPassword } = data;

  const { register, loading } = useContext(AuthContext);

  function registerHandler(e) {
    e.preventDefault();
    if (email === "" || password === "" || name === "") {
      alert("لطفا همه فیلد ها را وارد نمایید !! ");
    } else if (password === repeatPassword) {
      register({
        email,
        name,
        password,
      });
    } else {
      alert("Passwords dos't match ");
    }
  }

  useEffect(() => {
    if (localStorage.token) {
      navigate("/app/project-list");
    }
  }, []);

  return (
    // Register Form
    <div className='bg-slate-200 h-screen w-screen flex flex-col items-center justify-evenly'>
      {/* Logo  */}
      <div
        className='absolute top-1 flex flex-col items-center justify-between gap-2'
        style={{ maxWidth: "120px" }}
      >
        <img
          alt='Logo'
          src='https://cdn0.iconfinder.com/data/icons/octicons/1024/dashboard-512.png'
        />
        <span className='font-semibold text-blue-600'>Pomelatform</span>
      </div>
      <CCard className='shadow-md shadow-slate-400 w-11/12 md:w-7/12 lg:w-4/12'>
        <CCardBody>
          <div className='mb-4'>
            <h1 className='text-3xl'>ثبت نام</h1>
          </div>
          {/* Form */}
          <form>
            {/* Name */}
            <div className='mb-5 my-3 input-div'>
              <input
                required
                placeholder=' '
                onChange={(e) => setData({ ...data, name: e.target.value })}
                type={"text"}
                className='text-right form-input w-full border-t-0 border-x-0 border-b-2  outline-none shadow-none'
              />
              <label className='text-lg input-label'>
                <span>نام</span>
              </label>
            </div>
            {/* Email */}
            <div className='mb-5 my-3 input-div'>
              <input
                required
                placeholder=' '
                onChange={(e) => setData({ ...data, email: e.target.value })}
                type={"email"}
                className='text-right form-input w-full border-t-0 border-x-0 border-b-2  outline-none shadow-none'
              />
              <label className='text-lg input-label'>
                <span>ایمیل</span>
              </label>
            </div>
            {/* Password */}
            <div className='mb-5 my-3 input-div'>
              <input
                required
                minLength='6'
                placeholder=' '
                onChange={(e) => setData({ ...data, password: e.target.value })}
                type='password'
                className=' form-input w-full border-t-0 border-x-0 border-b-2  outline-none shadow-none'
              />
              <label className='text-lg input-label'>
                <span> گذر </span>
                <span style={{ transitionDelay: "100ms" }}>واژه</span>
              </label>
            </div>
            {/* Password Repeat */}
            <div className='mb-4 my-3 input-div'>
              <input
                required
                minLength='6'
                placeholder=' '
                onChange={(e) =>
                  setData({ ...data, repeatPassword: e.target.value })
                }
                type='password'
                className='text-right form-input w-full border-t-0 border-x-0 border-b-2  outline-none shadow-none'
              />
              <label className='text-lg input-label'>
                <span> تکرار </span> <span> گذر </span>
                <span style={{ transitionDelay: "100ms" }}>واژه</span>
              </label>
            </div>
            {/* Submit BTN */}
            <button
              type='submit'
              disabled={loading}
              onClick={registerHandler}
              className='w-full shadow-md shadow-blue-500 duration-100 active:scale-[0.97] bg-blue-600 text-white p-2 rounded hover:bg-blue-700'
            >
              {loading ? <CSpinner /> : "ثبت نام"}
            </button>
            <div className='my-3'>
              در حال حاظر دارای اکانت میباشید ؟{" "}
              <Link
                className='no-underline text-blue-900 hover:text-blue-600 font-bold'
                to='/login'
              >
                ورود به اکانت
              </Link>
            </div>
          </form>
        </CCardBody>
      </CCard>
    </div>
  );
};

export default Register;
