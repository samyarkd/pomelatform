import { CSpinner } from "@coreui/react-pro";
import React, { useContext } from "react";
import { Navigate } from "react-router-dom";
import AuthContext from "../../context/auth/AuthContext";
import RoutesContext from "../../context/Routes/RoutesContext";

function Loading() {
  return (
    <div className='flex  justify-center items-center h-screen w-screen'>
      <CSpinner />
    </div>
  );
}

function PrivateRoute({ children }) {
  const { isAuthenticated } = useContext(AuthContext);
  const { loading } = useContext(RoutesContext);

  return isAuthenticated || localStorage.token ? (
    loading ? (
      <Loading />
    ) : (
      children
    )
  ) : (
    <Navigate replace to='/login' />
  );
}

export default PrivateRoute;
