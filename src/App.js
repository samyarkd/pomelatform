/* eslint-disable react-hooks/exhaustive-deps */
import { useContext, useEffect } from "react";
import { Navigate, Route, Routes } from "react-router-dom";
import "./App.css";
import AppLayout from "./components/layout/app/AppLayout";
import ForgotP from "./components/pages/login/ForgotP";
import Login from "./components/pages/login/Login";
import Register from "./components/pages/Register/Register";
import PrivateRoute from "./components/privateRoutes/PrivateRoutes";
import PublicRoute from "./components/privateRoutes/PublicRoutes";
import AuthContext from "./context/auth/AuthContext";
import RoutesContext from "./context/Routes/RoutesContext";
import ChangeP from './components/pages/login/ChangeP';

function App() {
  const { setRoutes } = useContext(RoutesContext);
  const { user, loadUser } = useContext(AuthContext);

  useEffect(() => {
    if (user?.role_name) {
      setRoutes(user.role_name);
    } else if (localStorage.token) {
      loadUser();
    }
  }, [user]);
  return (
    <Routes>
      <Route
        path='/app/*'
        element={
          <PrivateRoute>
            <AppLayout />
          </PrivateRoute>
        }
      />
      <Route
        path='/login'
        element={
          <PublicRoute>
            <Login />
          </PublicRoute>
        }
      />
      <Route
        path='/forgot-password'
        element={
          <PublicRoute>
            <ForgotP />
          </PublicRoute>
        }
      />
      <Route
        path='/change-password'
        element={
          <PublicRoute>
            <ChangeP />
          </PublicRoute>
        }
      />
      <Route
        path='/register'
        element={
          <PublicRoute>
            <Register />
          </PublicRoute>
        }
      />

      <Route path='*' element={<Navigate to='/login' />} />
      <Route path='/' element={<Navigate to='/login' />} />
    </Routes>
  );
}

export default App;
